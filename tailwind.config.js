module.exports = {
  purge: {
    content: [
      './components/**/*.{vue,js}',
      './modules/**/*.{vue,js}',
      './layouts/**/*.vue',
      './pages/**/*.vue',
      './views/**/*.vue',
      './plugins/**/*.{js,ts}',
      './nuxt.config.{js,ts}',
    ],
  },
  theme: {
    extend: {
      colors: {
        primary: {
          500: '#0D359B',
        },
        secondary: {
          500: '#919BAF',
        },
        thirdary: {
          500: '#E0E0E0'
        },
        semantic: {
          success: '#0F9D58',
          warning: '#FBBC05',
          danger: '#FD2512',
          info: '#0038E5',
        }
      },
    },
  },
}
